const moment = require('moment')
const request = require('request')
const readline = require('readline')
const exec = require('child_process').exec
var CronJob = require('cron').CronJob
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const serialPort = new SerialPort('/dev/ttyUSB0', { baudRate: 115200 })
const parser = serialPort.pipe(new Readline({ delimiter: '\n' }))

const servidor = 'http://proyectoiot.atwebpages.com'
var interval

var json = {
  datetime: '',
  ubicacion: '',
  temperatura: '',
  humedad: '',
  calidad_del_aire: '',
  radiacion_uv: ''
}

//DEBUG: setTimeout(function(){ procesarDatos("1|2|3|4") }, 3000)

//Example: "PAYLOAD:ubicacion|temperatura|humedad|calidad_del_aire|radiacion_uv"
var linea = ''
function conectarSerial() {
  serialPort.on('open', function () {
    serialPort.on('data', function (data) {
      linea += data
      if(linea[linea.length-1] == '\n') {
        console.log(linea)
        if(linea.includes('PAYLOAD:')) {
          var payload = linea.substring(linea.indexOf(':')+1, linea.length-2)
          procesarDatos(payload)
        }
        linea = ''
      }
    })
  })

  serialPort.on('close', function() {
    console.log('Serial: Puerto cerrado')
    setTimeout(function() {
      conectarSerial()
    }, 2000)
  })

  serialPort.on('error', function (err) {
    console.error('Serial:', err)
    setTimeout(function() {
      conectarSerial()
    }, 2000)
  })
}
conectarSerial()

var lastUpdate = moment().unix()

function procesarDatos(payload) {
  lastUpdate = moment().unix()
  json.datetime = moment().format('YYYY-MM-DD HH:mm:ss')
  console.log("Payload:", payload)
  payload = payload.split('|')
  json.ubicacion = payload[0]
  json.temperatura = payload[1]
  json.humedad = payload[2]
  json.calidad_del_aire = payload[3]
  json.radiacion_uv = payload[4]
  enviarDatos(json)
}

function enviarDatos(data) {
  console.log('ENVIANDO...:', data)
  var options = {
    url: servidor + '/api/api.php',
    method: 'POST',
    timeout: 9000
  }

  options.json = data

  request(options, function(error, response, body) {
    if(error || body.status == 'error') {
      console.log('ENVIADO ERROR:', (error ? error : body))
    }else{
      console.log('ENVIADO OK:', body)
    }
  })
}

function restart(callback) {
  exec('pm2 restart all', function(error, stdout, stderr){ callback(stdout) })
}

function reboot(callback) {
  exec('sudo reboot', function(error, stdout, stderr){ callback(stdout) })
}

//Tareas
/*new CronJob('00 00 * * * *', function() {
  var diffTime = moment().diff(lastUpdate);
  var duration = moment.duration(diffTime);
  if(duration.minutes() >= 10) {
    restart(function(output) {
      console.log(output)
    })
  }
}, null, true, 'America/Lima')

new CronJob('00 15 * * * *', function() {
  var diffTime = moment().diff(lastUpdate);
  var duration = moment.duration(diffTime);
  if(duration.minutes() >= 10) {
    restart(function(output) {
      console.log(output)
    })
  }
}, null, true, 'America/Lima')

new CronJob('00 30 * * * *', function() {
  var diffTime = moment().diff(lastUpdate);
  var duration = moment.duration(diffTime);
  if(duration.minutes() >= 10) {
    restart(function(output) {
      console.log(output)
    })
  }
}, null, true, 'America/Lima')

new CronJob('00 45 * * * *', function() {
  var diffTime = moment().diff(lastUpdate);
  var duration = moment.duration(diffTime);
  if(duration.minutes() >= 10) {
    restart(function(output) {
      console.log(output)
    })
  }
}, null, true, 'America/Lima')

new CronJob('00 00 00 * * *', function() {
  reboot(function(output) {
    console.log(output)
  })
}, null, true, 'America/Lima')
*/
