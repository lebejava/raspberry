
#include "DHT.h" // libreria de sensor Temperatura
#define DHTPIN 14 // salida sensor Temperatura
#define pinOut A5 // salida sensor RU
#define pinRef3V3 A4 // saluda sensor RU
#define DHTTYPE DHT11

const String UBICACION = "ubicacion";

DHT dht(DHTPIN, DHTTYPE); // variable Temperatura
int sensorPinCA = A1; // salida sensor Calidad Aire
int sensorValorCA = 0; // inicializacion Temperatura

void setup()
{
  Serial.begin(115200);
  delay(500); // Retraso para permitir que el sistema arranque
  delay(1000); // Tiempo de espera antes de acceder al sensor
  pinMode(pinOut, INPUT); // sensor UV
  pinMode(pinRef3V3, INPUT); // sensor UV
  dht.begin();
}
 
void loop()
{
  // Procesos para sensor Temperatura
  //DHT.read11(dht_apin);
  sensorValorCA = analogRead(sensorPinCA);

  // Procesos para sensor RU
  // Lectura del valor anal�gico de ambas entradas,
  // para recuperar la subrutina llamada que devuelve
  // promedio de 8 mediciones
  int hodnotaUV = prumerAnalogRead(pinOut);
  int hodnotaRef3V3 = prumerAnalogRead(pinRef3V3);

  // Rec�lculo del voltaje de lectura del sensor UV y voltaje
  // de 3.3 V Arduino para el valor exacto del sensor UV
  float napetiOutUV = 3.3 / hodnotaRef3V3 * hodnotaUV;
  // conversi�n de voltaje del sensor UV a intensidad UV
  float intenzitaUV = prevodNapetiIntenzita(napetiOutUV, 0.96, 2.8, 0.0, 15.0);

  // Salida de informacion
  Serial.print("PAYLOAD:");
  Serial.print(UBICACION);
  Serial.print("|");
  Serial.print(dht.readTemperature());
  Serial.print("|");
  Serial.print(dht.readHumidity());
  Serial.print("|");
  Serial.print(sensorValorCA);
  /*Serial.println(" *PPM ");
  Serial.print(" Sensor ML8511 Voltaje: ");
  Serial.print(napetiOutUV);*/
  Serial.print("|"); // V, UV Intensidad (mW/cm^2): 
  Serial.println(intenzitaUV);
  delay(10000); // Esperar 5 segundos antes de acceder al sensor nuevamente.
} 

// subrutina para realizar 8 mediciones y devolver un valor promedio
int prumerAnalogRead(int pinToRead)
{
  byte numberOfReadings = 8;
  unsigned int runningValue = 0; 

  for(int x = 0 ; x < numberOfReadings ; x++)
    runningValue += analogRead(pinToRead);
  runningValue /= numberOfReadings;

  return(runningValue);  
}

// subrutina para convertir el valor medido
// de intensidad decimal a UV
float prevodNapetiIntenzita(float x, float in_min, float in_max,
float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
